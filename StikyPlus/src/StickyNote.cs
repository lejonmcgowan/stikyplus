﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using Windows.UI;
using Windows.UI.Composition;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Hosting;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using XamlBrewer.Uwp.Controls;
using Binding = System.ServiceModel.Channels.Binding;

namespace StikyPlus.src
{
    public class StickyNote
    {

        private Style noteStyle;
        public int Id { get; private set; }
        private bool inScene;
        private readonly MainPage pageRef;
        internal static readonly float SHADOW_OFFSET = 4.0f;

        private Control noteBase;
        public bool Initialized { get; private set; }

        private readonly MenuFlyoutItem[] themeChoices;

        public StickyNote(int id, Style noteStyle, MainPage pageRef)
        {
            this.Id = id;
            this.noteStyle = noteStyle;
            this.pageRef = pageRef;
            themeChoices = new MenuFlyoutItem[5];

            SetupThemeFlyouts();
        }

        private void SetupThemeFlyouts()
        {
            Color color = AppUtils.GetBackgroundFromStyle(AppUtils.GetNoteStyles("Yellow")[0]);

            MenuFlyoutItem yellow = new MenuFlyoutItem
            {
                Style = Application.Current.Resources["ColorFlyoutItem"] as Style,
                Foreground = new SolidColorBrush(color),
                Padding = new Thickness(5),
                Tag = "Yellow" + Id
            };

            color = AppUtils.GetBackgroundFromStyle(AppUtils.GetNoteStyles("Blue")[0]);
            MenuFlyoutItem blue = new MenuFlyoutItem
            {
                Style = Application.Current.Resources["ColorFlyoutItem"] as Style,
                Foreground = new SolidColorBrush(color),
                Padding = new Thickness(5),
                Tag = "Blue" + Id
            };

            color = AppUtils.GetBackgroundFromStyle(AppUtils.GetNoteStyles("Red")[0]);
            MenuFlyoutItem red = new MenuFlyoutItem
            {
                Style = Application.Current.Resources["ColorFlyoutItem"] as Style,
                Foreground = new SolidColorBrush(color),
                Padding = new Thickness(5),
                Tag = "Red" + Id
            };


            color = AppUtils.GetBackgroundFromStyle(AppUtils.GetNoteStyles("Green")[0]);
            MenuFlyoutItem green = new MenuFlyoutItem
            {
                Style = Application.Current.Resources["ColorFlyoutItem"] as Style,
                Foreground = new SolidColorBrush(color),
                Padding = new Thickness(5),
                Tag = "Green" + Id
            };

            color = AppUtils.GetBackgroundFromStyle(AppUtils.GetNoteStyles("Purple")[0]);
            MenuFlyoutItem purple = new MenuFlyoutItem
            {
                Style = Application.Current.Resources["ColorFlyoutItem"] as Style,
                Foreground = new SolidColorBrush(color),
                Padding = new Thickness(5),
                Tag = "Purple" + Id
            };

            themeChoices[0] = yellow;
            themeChoices[1] = blue;
            themeChoices[2] = red;
            themeChoices[3] = green;
            themeChoices[4] = purple;

            foreach (MenuFlyoutItem item in themeChoices)
                item.Click += pageRef.ChangeNoteColor;
        }

        private void init()
        {
            
            Grid noteGrid = new Grid
            {
                Name = "NoteGrid" + Id,
                MinWidth = 150,
                MinHeight = 150,
                Width = 200,
                Height = 200,
                Margin = new Thickness(0)
            };
            noteGrid.Tapped += pageRef.BringNoteToFront;

            RowDefinition headerDefinition = new RowDefinition();
            headerDefinition.Height = new GridLength(35, GridUnitType.Pixel);
            RowDefinition noteDefinition = new RowDefinition();
            headerDefinition.Height = new GridLength(1, GridUnitType.Auto);
            noteGrid.RowDefinitions.Add(headerDefinition);
            noteGrid.RowDefinitions.Add(noteDefinition);
            noteGrid.SizeChanged += updateShadowContent;

            //for the drop shadow, because UWP sucks. 
            Canvas shadowHolder = new Canvas()
            {
                Width =noteGrid.Width,
                Height = noteGrid.Height
            };
            shadowHolder.Name = "ShadowHolder" + Id;
            Grid.SetRowSpan(shadowHolder,2);
            SetupDropShadow(shadowHolder);
            noteGrid.Children.Add(shadowHolder);

            Grid noteHeader = new Grid();
            noteHeader.Name = "MenuStrip" + Id;
            noteHeader.Height = 35;
            Grid.SetRow(noteHeader, 0);
            noteHeader.Style = (Style)Application.Current.Resources["YellowHeaderTheme"];
            
            StackPanel leftAlign = new StackPanel();
            leftAlign.Name = "LeftStrip" + Id;
            leftAlign.Orientation = Orientation.Horizontal;
            leftAlign.HorizontalAlignment = HorizontalAlignment.Left;

            StackPanel rightAlign = new StackPanel();
            rightAlign.Name = "RightStrip" + Id;
            rightAlign.Orientation = Orientation.Horizontal;
            rightAlign.HorizontalAlignment = HorizontalAlignment.Right;

            noteHeader.Children.Add(leftAlign);
            noteHeader.Children.Add(rightAlign);

            Button noteAddButton = new Button();
            noteAddButton.Name = "AddButton" + Id;
            noteAddButton.Background = new SolidColorBrush(Colors.Transparent);
            noteAddButton.Width = 35;
            noteAddButton.Height = 35;
            noteAddButton.Padding = new Thickness(3);
            noteAddButton.BorderThickness = new Thickness(0);
            noteAddButton.Click += pageRef.AddNewStikcyNote;


            Image addImage = new Image()
            {
                Opacity = 0.7,
                Source = new BitmapImage(new Uri("ms-appx://StikyPlus/Assets/plus_icon.png"))
            };
            noteAddButton.Content = addImage;

            leftAlign.Children.Add(noteAddButton);

            Button noteThemeButton = new Button();
            noteThemeButton.Name = "ThemeButton" + Id;
            noteThemeButton.Background = new SolidColorBrush(Colors.Transparent);
            noteThemeButton.Width = 35;
            noteThemeButton.Height = 35;
            noteThemeButton.Padding = new Thickness(3);
            noteThemeButton.BorderThickness = new Thickness(0);

            Image themeImage = new Image
            {
                Source = new BitmapImage(new Uri("ms-appx://StikyPlus/Assets/paint_icon.png")),
                Opacity = 0.7
            };

            noteThemeButton.Content = themeImage;

            MenuFlyout themePickerDialog = new MenuFlyout
            {
                Placement = FlyoutPlacementMode.Bottom,
                MenuFlyoutPresenterStyle = Application.Current.Resources["HorizontalMenuPresenter"] as Style
            };

            IList<MenuFlyoutItemBase> items = themePickerDialog.Items ?? new List<MenuFlyoutItemBase>();

            foreach (MenuFlyoutItem item in themeChoices)
                items.Add(item);

            noteThemeButton.Flyout = themePickerDialog;
            leftAlign.Children.Add(noteThemeButton);

            Button noteDeleteButton = new Button()
            {
                Name = "DeleteButton" + Id,
                Background = new SolidColorBrush(Colors.Transparent),
                Width = 35,
                Height = 35,
                Padding = new Thickness(3),
                BorderThickness = new Thickness(0)
            };
            noteDeleteButton.Click += pageRef.RemoveNote;

            Image deleteImage = new Image()
            {
                Source = new BitmapImage(new Uri("ms-appx://StikyPlus/Assets/delete_icon.png")),
                Opacity = 0.7
            };

            noteDeleteButton.Content = deleteImage;

            rightAlign.Children.Add(noteDeleteButton);

            noteGrid.Children.Add(noteHeader);

            SingleKeyDownRichEditBox noteContent = new SingleKeyDownRichEditBox();
            Grid.SetRow(noteContent, 1);
            noteContent.Name = "TextEntry" + Id;
            noteContent.IsHoldingEnabled = false;
            noteContent.Style = noteStyle;
            noteContent.BorderThickness = new Thickness(0);
            noteContent.GotFocus += pageRef.BringNoteToFront;

            noteGrid.Children.Add(noteContent);

            Debug.WriteLine("num children before: " + noteGrid.Children.Count);
            foreach (Thumb thumb in makeResizeThumbs())
                noteGrid.Children.Add(thumb);

            Debug.WriteLine("num children after: " + noteGrid.Children.Count);

            //finally set the content
            FloatingContent root = new FloatingContent();
            root.Boundary = FloatingBoundary.Parent;
            root.Content = noteGrid;

            noteBase = root;

        }

        private void updateShadowContent(object sender, SizeChangedEventArgs e)
        {
            Panel panel = sender as Panel;
            if (panel != null)
            {
                Canvas shadowCanvas = panel.FindName("ShadowHolder" + Id) as Canvas;
                if (shadowCanvas != null)
                {
                    shadowCanvas.Width = e.NewSize.Width;
                    shadowCanvas.Height = e.NewSize.Height;

                    Visual visual =  ElementCompositionPreview.GetElementChildVisual(shadowCanvas);
                    visual.Size = new Vector2((float)e.NewSize.Width + 2 * SHADOW_OFFSET,(float)e.NewSize.Height + 2 * SHADOW_OFFSET);
                }
            }
        }

        private Thumb[] makeResizeThumbs()
        {
            Thumb[] thumbs = new Thumb[8];
            for (int i = 0; i < thumbs.Length; i++)
                thumbs[i] = new Thumb();

            //left
            thumbs[0].Name = "LeftThumb" + Id;
            Grid.SetRowSpan(thumbs[0], 2);
            thumbs[0].HorizontalAlignment = HorizontalAlignment.Left;
            thumbs[0].Width = 5;

            //right
            thumbs[1].Name = "RightThumb" + Id;
            Grid.SetRowSpan(thumbs[1], 2);
            thumbs[1].HorizontalAlignment = HorizontalAlignment.Right;
            thumbs[1].Width = 5;


            //top
            thumbs[2].Name = "TopThumb" + Id;
            Grid.SetRow(thumbs[2], 0);
            thumbs[2].VerticalAlignment = VerticalAlignment.Top;
            thumbs[2].Height = 5;

            //bottom
            thumbs[3].Name = "BottomThumb" + Id;
            Grid.SetRow(thumbs[3], 1);
            thumbs[3].VerticalAlignment = VerticalAlignment.Bottom;
            thumbs[3].Height = 5;

            //topleft
            thumbs[4].Name = "TopLeftThumb" + Id;
            thumbs[4].HorizontalAlignment = HorizontalAlignment.Left;
            thumbs[4].VerticalAlignment = VerticalAlignment.Top;
            thumbs[4].Width = 10;
            thumbs[4].Height = 10;

            //topright
            thumbs[5].Name = "TopRightThumb" + Id;
            thumbs[5].HorizontalAlignment = HorizontalAlignment.Right;
            thumbs[5].VerticalAlignment = VerticalAlignment.Top;
            thumbs[5].Width = 10;
            thumbs[5].Height = 10;

            //bottomleft
            thumbs[6].Name = "BottomLeftThumb" + Id;
            thumbs[6].HorizontalAlignment = HorizontalAlignment.Left;
            thumbs[6].VerticalAlignment = VerticalAlignment.Bottom;
            thumbs[6].Width = 10;
            thumbs[6].Height = 10;
            Grid.SetRow(thumbs[6], 1);

            //bottomright
            thumbs[7].Name = "BottomRightThumb" + Id;
            thumbs[7].HorizontalAlignment = HorizontalAlignment.Right;
            thumbs[7].VerticalAlignment = VerticalAlignment.Bottom;
            thumbs[7].Width = 10;
            thumbs[7].Height = 10;
            Grid.SetRow(thumbs[7], 1);

            foreach (Thumb thumb in thumbs)
            {
                thumb.IsHoldingEnabled = false;
                thumb.DragDelta += pageRef.ResizeNoteEvent;
                thumb.PointerEntered += pageRef.UpdateCursor;
                thumb.PointerExited += pageRef.ResetCursor;
                thumb.PointerReleased += pageRef.ResetCursor;
                thumb.Style = App.Current.Resources["InvisibleThumb"] as Style;
                //                    thumb.Background = new SolidColorBrush(Colors.Blue);
            }


            return thumbs;
        }

        public void AddNoteToScene(Panel parent, float x, float y)
        {
            if (!inScene)
            {
                if (!Initialized)
                {
                    init();
                    Initialized = true;
                    noteBase.SetValue(Canvas.LeftProperty, x);
                    noteBase.SetValue(Canvas.TopProperty, y);
                    noteBase.Name = "Note" + Id;
                    parent.Children.Add(noteBase);
                    Debug.WriteLine("Note " + Id + " Added to Scene");

                    inScene = true;
                }

            }
        }

        public void RemoveNoteFromScene(Panel parent)
        {
            if (inScene)
            {
                FloatingContent child = parent.FindName("Note" + Id) as FloatingContent;

                if (child != null)
                {
                    parent.Children.Remove(child);
                    Initialized = false;
                    inScene = false;
                }

            }
        }

        internal static  void SetupDropShadow(Panel parent)
        {
            var compositor = ElementCompositionPreview.GetElementVisual(parent).Compositor;
            var spriteVisual = compositor.CreateSpriteVisual();
            //intellisense wtf?
            spriteVisual.Offset = new Vector3(-SHADOW_OFFSET,-SHADOW_OFFSET, 0);
            spriteVisual.Size = new Vector2((float)parent.Width + 2 * SHADOW_OFFSET, (float)parent.Height + 2 * SHADOW_OFFSET);
            var dropShadow = compositor.CreateDropShadow();
            //here too
            dropShadow.BlurRadius = 2.0f;
            dropShadow.Color = Colors.Black;
            dropShadow.Opacity = 0.15f;
            spriteVisual.Shadow = dropShadow;
            ElementCompositionPreview.SetElementChildVisual(parent, spriteVisual);
        }


        public Control GetRoot()
        {
            return noteBase;
        }

        public string GetText()
        {
            if (!Initialized)
                return null;
            RichEditBox textEntry = noteBase.FindName("TextEntry" + Id) as RichEditBox;
            string textContent;
            textEntry.Document.GetText(TextGetOptions.None, out textContent);
            return textContent;
        }
    }
}
