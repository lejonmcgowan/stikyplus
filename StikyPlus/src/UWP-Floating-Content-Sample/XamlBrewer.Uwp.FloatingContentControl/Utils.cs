﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace XamlBrewer.Uwp.FloatingContentControl
{
    class FloatingUtils
    {
        public static List<UIElement> GetAllChildren(UIElement root)
        {
            List<UIElement> elements = new List<UIElement>();

            if (root != null)
            {
                Stack<UIElement> currentElements = new Stack<UIElement>();
                currentElements.Push(root);
                while (currentElements.Count > 0)
                {
                    UIElement element = currentElements.Pop();

                    if (element != root)
                    {
                        elements.Add(element);
                    }

                    int elemChildCount = VisualTreeHelper.GetChildrenCount(element);
                    //Debug.WriteLine("checking " + elemChildCount + " in " + element.ToString());
                    for (int i = 0; i < elemChildCount; i++)
                    {
                        UIElement subElement = VisualTreeHelper.GetChild(element, i) as UIElement;
                        if (subElement != null)
                        {
                            currentElements.Push(subElement);
                        }
                    }
                }
            }

            return elements;
        }
    }
}
