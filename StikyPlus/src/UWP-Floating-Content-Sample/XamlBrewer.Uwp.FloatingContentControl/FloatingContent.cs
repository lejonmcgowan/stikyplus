﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Windows.Foundation;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Shapes;
using Microsoft.VisualBasic.CompilerServices;
using XamlBrewer.Uwp.FloatingContentControl;

namespace XamlBrewer.Uwp.Controls
{
    /// <summary>
    /// A Content Control that can be dragged around.
    /// </summary>
    [TemplatePart(Name = BorderPartName, Type = typeof(Border))]
    [TemplatePart(Name = OverlayPartName, Type = typeof(UIElement))]
    public class FloatingContent : ContentControl
    {
        private const string BorderPartName = "PART_Border";
        private const string OverlayPartName = "PART_Overlay";
        private List<UIElement> disablElements = new List<UIElement>();
        private bool disableInit;
        private bool shouldMove = true;
        private Border border;
        private Point oldPosition;
        private UIElement overlay;

        public static readonly DependencyProperty BoundaryProperty =
            DependencyProperty.Register(
                "Boundary",
                typeof(FloatingBoundary),
                typeof(FloatingContent),
                new PropertyMetadata(FloatingBoundary.None));

        /// <summary>
        /// Initializes a new instance of the <see cref="FloatingContent"/> class.
        /// </summary>
        public FloatingContent()
        {
            this.DefaultStyleKey = typeof(FloatingContent);
        }

        public FloatingBoundary Boundary
        {
            get { return (FloatingBoundary)GetValue(BoundaryProperty); }
            set { SetValue(BoundaryProperty, value); }
        }

        /// <summary>
        /// Invoked whenever application code or internal processes (such as a rebuilding layout pass) call ApplyTemplate.
        /// In simplest terms, this means the method is called just before a UI element displays in your app.
        /// Override this method to influence the default post-template logic of a class.
        /// </summary>
        protected override void OnApplyTemplate()
        {
            // Border
            this.border = this.GetTemplateChild(BorderPartName) as Border;

            

            if (this.border != null)
            {
                this.border.ManipulationStarted += Border_ManipulationStarted;
                this.border.ManipulationDelta += Border_ManipulationDelta;
                this.border.ManipulationCompleted += Border_ManipulationCompleted;
                this.border.Tapped += Border_Tapped;
                this.border.PointerEntered += Border_PointerEntered;
                
                //gotta do it this way to ensure that event triggers, regardless of children (RichEditBox :(  )
                border.AddHandler(PointerPressedEvent,new PointerEventHandler(Border_PointerPressed),true);

                // Move Canvas properties from control to border.
                Canvas.SetLeft(this.border, Canvas.GetLeft(this));
                Canvas.SetLeft(this, 0);
                Canvas.SetTop(this.border, Canvas.GetTop(this));
                Canvas.SetTop(this, 0);

                // Move Margin to border.
                this.border.Padding = this.Margin;
                this.Margin = new Thickness(0);
            }
            else
            {
                // Exception
                throw new Exception("Floating Control Style has no Border.");
            }

            // Overlay
            this.overlay = GetTemplateChild(OverlayPartName) as UIElement;

            this.Loaded += Floating_Loaded;
        }

        private void Border_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            //Debug.WriteLine("Pointer pressed");

            if (!disableInit)
            {
                updateDisabledElements();
                disableInit = true;
            }

            //MY CODE. checked clicked element for whether or not it should move the view
            UIElement clickedElement = sender as UIElement;
            if (clickedElement != null)
            {

                Point absolutePosition = e.GetCurrentPoint(Window.Current.Content).Position;
                //Debug.WriteLine("POINTER at position " + absolutePosition.ToString());
                IEnumerable<UIElement> touchElements = VisualTreeHelper.FindElementsInHostCoordinates(absolutePosition, this);
                //Debug.WriteLine(touchElements.Count() + " elements captured");
                foreach (UIElement element in touchElements)
                {
                    //Debug.WriteLine(element.ToString() + " at position " + absolutePosition.ToString());
                    if (disablElements.Contains(element))
                    {
                     //   Debug.WriteLine("Disabling...");
                        shouldMove = false;
                        break;
                    }
                }

            }
            else
            {
                Debug.WriteLine("FAIL WHALE. sender is not a UIElement");
            }

        }

        private void updateDisabledElements()
        {
            disablElements.Clear();

            List<UIElement> elements = FloatingUtils.GetAllChildren(this.border);
           // Debug.WriteLine("checking " + elements.Count + " children");

            foreach (UIElement element in elements)
            {
                //Debug.WriteLine("checking " + element.ToString());
                if (element != null && !element.IsHoldingEnabled)
                {
                  //  Debug.WriteLine("Added " + element.ToString());
                    disablElements.Add(element);
                }
            }
        }

        private void Border_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            //FlashOverlay();
        }

        private void Border_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //FlashOverlay();
            //Debug.WriteLine("tapped");
        }

        private void Border_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            if (this.overlay != null)
            {
                //disableOverlay. Don't need it
                var ani = new DoubleAnimation()
                {
                    From = 0.0,
                    To = 0.0,
                    Duration = new Duration(TimeSpan.FromSeconds(1.5))
                };

                var storyBoard = new Storyboard();
                storyBoard.Children.Add(ani);
                Storyboard.SetTarget(ani, overlay);
                ani.SetValue(Storyboard.TargetPropertyProperty, "Opacity");
                //storyBoard.Begin();
            }
            oldPosition = new Point(Canvas.GetLeft(border), Canvas.GetTop(border));
        }

        private void Border_ManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {
            shouldMove = true;
            if (this.overlay != null)
            {
                var ani = new DoubleAnimation()
                {
                    From = 0.0,
                    To = 0.0,
                    Duration = new Duration(TimeSpan.FromSeconds(0.25))
                };
                var storyBoard = new Storyboard();
                storyBoard.Children.Add(ani);
                Storyboard.SetTarget(ani, overlay);
                ani.SetValue(Storyboard.TargetPropertyProperty, "Opacity");
                //storyBoard.Begin();
            }
        }

        private void Floating_Loaded(object sender, RoutedEventArgs e)
        {
            FrameworkElement el = GetClosestParentWithSize(this);
            if (el == null)
            {
                return;
            }

            el.SizeChanged += Floating_SizeChanged;
        }

        private void Floating_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var left = Canvas.GetLeft(this.border);
            var top = Canvas.GetTop(this.border);

            Rect rect = new Rect(left, top, this.border.ActualWidth, this.border.ActualHeight);

            AdjustCanvasPosition(rect);
        }

        private void Border_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            
            var left = Canvas.GetLeft(this.border) + e.Delta.Translation.X;
            var top = Canvas.GetTop(this.border) + e.Delta.Translation.Y;
            Rect rect = new Rect(left, top, this.border.ActualWidth, this.border.ActualHeight);

            e.Handled = !shouldMove;

            if (!e.Handled)
            {
                var moved = AdjustCanvasPosition(rect);
            }

            // Not intuitive:
            //if (!moved)
            //{
            //    // We hit the boundary. Stop the inertia.
            //    e.Complete();
            //}
        }

        /// <summary>
        /// Adjusts the canvas position according to the IsBoundBy* properties.
        /// </summary>
        private bool AdjustCanvasPosition(Rect rect)
        {
            // Free floating.
            if (this.Boundary == FloatingBoundary.None)
            {
                Canvas.SetLeft(this.border, rect.Left);
                Canvas.SetTop(this.border, rect.Top);

                return true;
            }

            FrameworkElement el = GetClosestParentWithSize(this);

            // No parent
            if (el == null)
            {
                // We probably never get here.
                return false;
            }

            var position = new Point(rect.Left, rect.Top); ;

            if (this.Boundary == FloatingBoundary.Parent)
            {
                Rect parentRect = new Rect(0, 0, el.ActualWidth, el.ActualHeight);
                position = AdjustedPosition(rect, parentRect);
            }

            if (this.Boundary == FloatingBoundary.Window)
            {
                var ttv = el.TransformToVisual(Window.Current.Content);
                var topLeft = ttv.TransformPoint(new Point(0, 0));
                Rect parentRect = new Rect(topLeft.X, topLeft.Y, Window.Current.Bounds.Width - topLeft.X, Window.Current.Bounds.Height - topLeft.Y);
                position = AdjustedPosition(rect, parentRect);
            }

            // Set new position
            Canvas.SetLeft(this.border, position.X);
            Canvas.SetTop(this.border, position.Y);

            return position == new Point(rect.Left, rect.Top);
        }

        /// <summary>
        /// Returns the adjusted the topleft position of a rectangle so that is stays within a parent rectangle.
        /// </summary>
        /// <param name="rect">The rectangle.</param>
        /// <param name="parentRect">The parent rectangle.</param>
        /// <returns></returns>
        private Point AdjustedPosition(Rect rect, Rect parentRect)
        {
            var left = rect.Left;
            var top = rect.Top;

            if (left < -parentRect.Left)
            {
                // Left boundary hit.
                left = -parentRect.Left;
            }
            else if (left + rect.Width > parentRect.Width)
            {
                // Right boundary hit.
                left = parentRect.Width - rect.Width;
            }

            if (top < -parentRect.Top)
            {
                // Top hit.
                top = -parentRect.Top;
            }
            else if (top + rect.Height > parentRect.Height)
            {
                // Bottom hit.
                top = parentRect.Height - rect.Height;
            }

            return new Point(left, top);
        }

        /// <summary>
        /// Gets the closest parent with a real size.
        /// </summary>
        private FrameworkElement GetClosestParentWithSize(FrameworkElement element)
        {
            while (element != null && (element.ActualHeight == 0 || element.ActualWidth == 0))
            {
                // Crawl up the Visual Tree.
                element = element.Parent as FrameworkElement;
            }

            return element;
        }
    }
}
