﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

/**
 *  credit to: https://github.com/ruffin--/UWPBox/blob/master/UWPBox/org/rufwork/UI/widgets/UWPBox.cs
 *  
 *  for acknoledging the same tab issues I've been having
 * 
 */
namespace StikyPlus
{
    //don't really know why the double CTRL+I shortut is being triggered, but am lazy (and it isn't italics, like what I want). Let's just take it out of consideration
    public class SingleKeyDownRichEditBox : RichEditBox
    {
        private static readonly int minFontSize = 9;
        private static readonly int maxFontSize = 25;
        private readonly float defaultFontSize;

        public SingleKeyDownRichEditBox(): base()
        {
            this.KeyDown += this.KeyDownHandler;
            
            ITextCharacterFormat format = Document.Selection.CharacterFormat;
            defaultFontSize = format.Size;
        }

        public virtual async void KeyDownHandler(object sender, KeyRoutedEventArgs e)
        {
            bool isCtrlDown = Window.Current.CoreWindow.GetKeyState(VirtualKey.Control).HasFlag(CoreVirtualKeyStates.Down);
            bool isShiftDown = Window.Current.CoreWindow.GetKeyState(VirtualKey.Shift).HasFlag(CoreVirtualKeyStates.Down);
            bool isAltDown = Window.Current.CoreWindow.GetKeyState(VirtualKey.LeftMenu).HasFlag(CoreVirtualKeyStates.Down)
                || Window.Current.CoreWindow.GetKeyState(VirtualKey.RightMenu).HasFlag(CoreVirtualKeyStates.Down);

            try
            {
                switch (e.OriginalKey)
                {
                    case VirtualKey.I:
                        if (isCtrlDown)
                        {
                            e.Handled = true;
                            ToggleNoteItalics();
                        }
                        break;
                    case VirtualKey.B:
                        if (isCtrlDown)
                        {
                            e.Handled = true;
                            ToggleNoteBold();
                        }
                        break;
                    case VirtualKey.U:
                        if (isCtrlDown)
                        {
                            e.Handled = true;
                            ToggleNoteUnderline();
                        }
                        break;
                    case VirtualKey.T:
                        if (isCtrlDown)
                        {
                            e.Handled = true;
                            ToggleNoteStrikeThrough();
                        }
                        break;
                    //comma key, no freaking clue wtf it is called. Thanks MSFT
                    case (VirtualKey)189:
                        if (isCtrlDown && isShiftDown)
                        {
                            e.Handled = true;
                            DecreaseFontSize();
                        }
                        break;
                    //period key
                    case (VirtualKey)190:
                        if (isCtrlDown && isShiftDown)
                        {
                            e.Handled = true;
                            IncreaseFontSize();
                        }
                        break;
                    // '/' key, the one next to the others
                    //period key
                    case (VirtualKey)191:
                        if (isCtrlDown && isShiftDown)
                        {
                            e.Handled = true;
                            ResetFontSize();
                        }
                        break;
                    default:
                        e.Handled = false;
                        break;
                }
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine("FAIL WHALE IN OVERRIDE OF RICHTEXTBOX");

            }

        }


        private void DecreaseFontSize()
        {
            RichEditBox textEntry = this;
            ITextDocument noteText = textEntry.Document;

            ITextSelection selectedText = noteText.Selection;
            if (selectedText != null)
            {
                ITextCharacterFormat charFormatting = selectedText.CharacterFormat;
                if(charFormatting.Size > minFontSize)
                    charFormatting.Size -= 1;
                selectedText.CharacterFormat = charFormatting;
            }

            Debug.WriteLine("Text decreased");
        }

        private void IncreaseFontSize()
        {
            RichEditBox textEntry = this;
            ITextDocument noteText = textEntry.Document;

            ITextSelection selectedText = noteText.Selection;
            if (selectedText != null)
            {
                ITextCharacterFormat charFormatting = selectedText.CharacterFormat;
                if(charFormatting.Size < maxFontSize)
                    charFormatting.Size += 1;
                selectedText.CharacterFormat = charFormatting;
            }

            Debug.WriteLine("Text increased");
        }

        private void ResetFontSize()
        {
            RichEditBox textEntry = this;
            ITextDocument noteText = textEntry.Document;

            ITextSelection selectedText = noteText.Selection;
            if (selectedText != null)
            {
                ITextCharacterFormat charFormatting = selectedText.CharacterFormat;
                charFormatting.Size = defaultFontSize;
                selectedText.CharacterFormat = charFormatting;
            }

            Debug.WriteLine("Text reset");
        }

        private void ToggleNoteBold()
        {
            RichEditBox textEntry = this;
            ITextDocument noteText = textEntry.Document;

            ITextSelection selectedText = noteText.Selection;
            if (selectedText != null)
            {
                ITextCharacterFormat charFormatting = selectedText.CharacterFormat;
                charFormatting.Bold = FormatEffect.Toggle;
                selectedText.CharacterFormat = charFormatting;
            }

        }

        private void ToggleNoteItalics()
        {
            RichEditBox textEntry = this;
            ITextDocument noteText = textEntry.Document;

            ITextSelection selectedText = noteText.Selection;
            if (selectedText != null)
            {
                ITextCharacterFormat charFormatting = selectedText.CharacterFormat;
                charFormatting.Italic = FormatEffect.Toggle;
                selectedText.CharacterFormat = charFormatting;
            }

        }

        private void ToggleNoteStrikeThrough()
        {
            RichEditBox textEntry = this;
            ITextDocument noteText = textEntry.Document;

            ITextSelection selectedText = noteText.Selection;
            if (selectedText != null)
            {
                ITextCharacterFormat charFormatting = selectedText.CharacterFormat;
                charFormatting.Strikethrough = FormatEffect.Toggle;
                selectedText.CharacterFormat = charFormatting;
            }

        }

        private void ToggleNoteUnderline()
        {
            RichEditBox textEntry = this;
            ITextDocument noteText = textEntry.Document;

            ITextSelection selectedText = noteText.Selection;
            if (selectedText != null)
            {
                ITextCharacterFormat charFormatting = selectedText.CharacterFormat;
                charFormatting.Underline = charFormatting.Underline != UnderlineType.None ? UnderlineType.None : UnderlineType.Single;
                selectedText.CharacterFormat = charFormatting;
            }

        }
    }

    
}
