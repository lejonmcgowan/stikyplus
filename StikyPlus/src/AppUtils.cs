﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace StikyPlus.src
{
    class AppUtils
    {
        public static Style[] GetNoteStyles(string colorName)
        {
            Style[] styles = new Style[2];
            string noteTextStyleName;
            string noteHeaderThemeStyleName;

            switch (colorName.ToUpper())
            {
                case "YELLOW":
                    noteHeaderThemeStyleName = "YellowHeaderTheme";
                    noteTextStyleName = "YellowNoteTheme";
                    break;
                case "BLUE":
                    noteHeaderThemeStyleName = "BlueHeaderTheme";
                    noteTextStyleName = "BlueNoteTheme";
                    break;
                case "RED":
                    noteHeaderThemeStyleName = "RedHeaderTheme";
                    noteTextStyleName = "RedNoteTheme";
                    break;
                case "PURPLE":
                    noteHeaderThemeStyleName = "PurpleHeaderTheme";
                    noteTextStyleName = "PurpleNoteTheme";
                    break;
                case "GREEN":
                    noteHeaderThemeStyleName = "GreenHeaderTheme";
                    noteTextStyleName = "GreenNoteTheme";
                    break;
                default:
                    noteHeaderThemeStyleName = "";
                    noteTextStyleName = "";
                    break;
            }
            styles[0] = Application.Current.Resources[noteHeaderThemeStyleName] as Style;
            styles[1] = Application.Current.Resources[noteTextStyleName] as Style;

            if (styles[0] == null)
                Debug.WriteLine("Failed to find style for note HeaderTheme");
            if (styles[1] == null)
                Debug.WriteLine("Failed to find style for note textbox");
            return styles;

        }
        /**
         * just a way to separate stuff like "textEntry23" into "textentry" and "23"
         * if there are no numbers, it will return the string as the first part, and empty in the second
         */
        public static string[] splitStringFromID(string input)
        {
            string[] result = new string[2];
            int pivot = -1;
            for (int i = 0; i < input.Length && pivot < 0; i++)
            {
                char letter = input[i];
                if (System.Char.IsDigit(letter))
                    pivot = i;
            }
            if (pivot < 0)
                pivot = input.Length;

            result[0] = input.Substring(0, pivot);
            result[1] = input.Substring(pivot);
            return result;
        }

        public static Color GetBackgroundFromStyle(Style style)
        {
            foreach (Setter setter in style.Setters)
            {
                SolidColorBrush brush = setter.Value as SolidColorBrush;
                if (brush != null)
                    return brush.Color;
            }
            return Colors.Transparent;
        }
    }
}
