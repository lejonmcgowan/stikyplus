﻿
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.Devices.Input;
using Windows.Foundation;
using Windows.Storage;
using Windows.System;
using Windows.UI;
using Windows.UI.Composition;
using Windows.UI.Core;
using Windows.UI.Input;
using Windows.UI.Text;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Hosting;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Microsoft.Graphics.Canvas;
using StikyPlus.src;
using Microsoft.Graphics.Canvas.Effects;
using XamlBrewer.Uwp.Controls;

namespace StikyPlus
{
    public sealed partial class MainPage : Page
    {
        private static readonly CoreCursor horCursor = new CoreCursor(CoreCursorType.SizeWestEast, 1);
        private static readonly CoreCursor vertCursor = new CoreCursor(CoreCursorType.SizeNorthSouth, 2);
        //top right and bottom left
        private static readonly CoreCursor diagCursor1 = new CoreCursor(CoreCursorType.SizeNortheastSouthwest, 3);
        //top left and bottom right
        private static readonly CoreCursor diagCursor2 = new CoreCursor(CoreCursorType.SizeNorthwestSoutheast, 4);
        private static readonly CoreCursor defaultCursor = new CoreCursor(CoreCursorType.Arrow, 0);

        private static readonly int INIT_NOTE_RANGE = 200;
        List<StickyNote> notes = new List<StickyNote>();

        private UIElement selectedElement;

        private bool ctrlPressed = false;
        private bool addNoteHandled;
        private int noteCount = 1;

        public MainPage()
        {
            this.InitializeComponent();
            CoreWindow.GetForCurrentThread().KeyDown += RootKeyProcess;

            for (int i = 0; i < INIT_NOTE_RANGE; i++)
                notes.Add(new StickyNote(i, App.Current.Resources["YellowNoteTheme"] as Style, this));

            

            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size(500,500));
            ApplicationView.PreferredLaunchViewSize = new Size(1280,720);

            //make app take up title bar space
            ApplicationViewTitleBar formattableTitleBar = ApplicationView.GetForCurrentView().TitleBar;
            formattableTitleBar.ButtonBackgroundColor = Colors.SaddleBrown;
            CoreApplicationViewTitleBar coreTitleBar = CoreApplication.GetCurrentView().TitleBar;
            coreTitleBar.ExtendViewIntoTitleBar = true;
        }

        

        //event methods and stuff

        private void OnPageSizeInit(object sender, SizeChangedEventArgs e)
        {
            Grid filterGrid = FindName("FilterGrid") as Grid;
            Canvas parent = FindName("BaseCanvas") as Canvas;
            double newMarginX = parent.ActualWidth / 30;
            double newMarginY = parent.ActualWidth / 30;
            parent.Margin = new Thickness(newMarginX,newMarginY, newMarginX, newMarginY);
            filterGrid.Width = parent.ActualWidth;

            Panel panel = FindName("ShadowHolder") as Panel;
            StickyNote.SetupDropShadow(panel);
            UpdateShadow(panel);

            FloatingContent helpNote = FindName("Note") as FloatingContent;

            setDefaultHelpText();
        }

        private async void setDefaultHelpText()
        {
            StorageFolder appInstalledFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            Windows.Storage.StorageFile defaultFile = await appInstalledFolder.GetFileAsync("helpText.rtf");

            if (defaultFile != null)
            {
                try
                {
                    Windows.Storage.Streams.IRandomAccessStream randAccStream =
                        await defaultFile.OpenAsync(Windows.Storage.FileAccessMode.Read);
                    RichEditBox helpEditBox = FindName("TextEntry") as RichEditBox;
                    // Load the file into the Document property of the RichEditBox.
                    helpEditBox.Document.LoadFromStream(Windows.UI.Text.TextSetOptions.FormatRtf, randAccStream);
                    helpEditBox.IsReadOnly = true;
                }
                catch (Exception)
                {
                    ContentDialog errorDialog = new ContentDialog()
                    {
                        Title = "File open error",
                        Content = "Sorry, I couldn't open the file.",
                        PrimaryButtonText = "Ok"
                    };

                    await errorDialog.ShowAsync();
                }
            }
            else
            {
                Debug.WriteLine("FAIL WHALE: cannot open default text file");
            }
        }


        public int FindNextFreeNote()
        {
            foreach (StickyNote note in notes)
            {
                if (!note.Initialized)
                    return note.Id;
            }
            //no free note found, add new one
            int notesCount = notes.Count;
            notes.Add(new StickyNote(notesCount, App.Current.Resources["YellowNoteTheme"] as Style, this));
            return notesCount;
        }

        internal void AddNewStikcyNote(object sender, RoutedEventArgs e)
        {
            int newNoteIndex = FindNextFreeNote();
            StickyNote newNote = notes[newNoteIndex];
            Canvas parent = FindName("BaseCanvas") as Canvas;
            newNote.AddNoteToScene(parent, 100 + new Random().Next(-30, 30), 100 + new Random().Next(-30, 30));
            noteCount++;
            Button element = sender as Button;
            if (element != null)
            {
                //make sure new note is in front
                string id = AppUtils.splitStringFromID(element.Name)[1];
                FloatingContent noteClicked = FindName("Note" + id) as FloatingContent;
                Canvas.SetZIndex(noteClicked,-1);
                Canvas.SetZIndex(newNote.GetRoot(), 1);
                selectedElement = newNote.GetRoot();
                Debug.WriteLine("Note " + (newNote.Id) + " in front");
            }
            //so that the "BringNoteToFront" method knows that this has been handled elsewhere
            addNoteHandled = true;

        }

        private void openThemeDialog(object sender, RoutedEventArgs e)
        {

        }

        private void ResizeNoteLeft(FrameworkElement note, Control parent, double delta)
        {
            double oldLeft = (double) parent.GetValue(Canvas.LeftProperty);

            double oldWidth = note.Width;
            double finalWidth = note.Width - delta;

            finalWidth = finalWidth < note.MinWidth
                ? note.MinWidth
                : finalWidth > note.MaxWidth ? note.MaxWidth : finalWidth;

            //sticky note must move OPPosite to the width change. e.g. dragging left means decreasing the left and increating the width
            parent.SetValue(Canvas.LeftProperty, oldLeft + (oldWidth - finalWidth));
            note.Width = finalWidth;
        }

        private void ResizeNoteRight(FrameworkElement note, double delta)
        {
            double finalWidth = note.Width + delta;

            finalWidth = finalWidth < note.MinWidth
                ? note.MinWidth
                : finalWidth > note.MaxWidth ? note.MaxWidth : finalWidth;

            //sticky note must move OPPosite to the width change. e.g. dragging left means decreasing the left and increating the width
            note.Width = finalWidth;
        }

        private void ResizeNoteTop(FrameworkElement note, Control parent, double delta)
        {
            double oldTop = (double) parent.GetValue(Canvas.TopProperty);

            double oldHeight = note.Height;
            double finalHeight = note.Height - delta;

            finalHeight = finalHeight < note.MinHeight
                ? note.MinHeight
                : finalHeight > note.MaxHeight ? note.MaxHeight : finalHeight;

            //sticky note must move OPPosite to the height change. e.g. dragging up means decreasing the top (move up) and increasing the height
            parent.SetValue(Canvas.TopProperty, oldTop + (oldHeight - finalHeight));
            note.Height = finalHeight;
        }

        private void ResizeNoteBottom(FrameworkElement note, double delta)
        {
            double finalHeight = note.Height + delta;

            finalHeight = finalHeight < note.MinHeight
                ? note.MinHeight
                : finalHeight > note.MaxHeight ? note.MaxHeight : finalHeight;

            //sticky note must move OPPosite to the height change. e.g. dragging up means decreasing the top (move up) and increasing the height
            note.Height = finalHeight;
        }


        internal void UpdateCursor(object sender, PointerRoutedEventArgs e)
        {
            Thumb thumb = sender as Thumb;
            string name = thumb.Name.ToUpper();
            if (name.Contains("THUMB"))
            {
                string subname = name.Substring(0, name.IndexOf("THUMB"));
                switch (subname)
                {
                    case "TOPLEFT":
                        Window.Current.CoreWindow.PointerCursor = diagCursor2;
                        break;
                    case "BOTTOMRIGHT":
                        Window.Current.CoreWindow.PointerCursor = diagCursor2;
                        break;
                    case "BOTTOMLEFT":
                        Window.Current.CoreWindow.PointerCursor = diagCursor1;
                        break;
                    case "TOPRIGHT":
                        Window.Current.CoreWindow.PointerCursor = diagCursor1;
                        break;
                    case "LEFT":
                        Window.Current.CoreWindow.PointerCursor = horCursor;
                        break;
                    case "RIGHT":
                        Window.Current.CoreWindow.PointerCursor = horCursor;
                        break;
                    case "BOTTOM":
                        Window.Current.CoreWindow.PointerCursor = vertCursor;
                        break;
                    case "TOP":
                        Window.Current.CoreWindow.PointerCursor = vertCursor;
                        break;
                    default:
                        Window.Current.CoreWindow.PointerCursor = defaultCursor;
                        break;
                }
            }
        }

        internal void ResetCursor(object sender, PointerRoutedEventArgs e)
        {
            PointerDeviceType pointerPointerDeviceType = e.Pointer.PointerDeviceType;

            PointerPoint currentPoint = e.GetCurrentPoint(null);
            //if it is pressed, the pointerreleased will work
            if (pointerPointerDeviceType == PointerDeviceType.Mouse && !currentPoint.Properties.IsLeftButtonPressed)
            {
                Window.Current.CoreWindow.PointerCursor = defaultCursor;
            }
        }


        internal void ResizeNoteEvent(object sender, DragDeltaEventArgs e)
        {
            Thumb thumb = sender as Thumb;
            FrameworkElement note = thumb?.Parent as FrameworkElement;

            if (thumb != null)
            {
                Control parent = note.Parent as Control;

                string name = thumb.Name.ToUpper();
                if (name.Contains("THUMB"))
                {
                    string subname = name.Substring(0, name.IndexOf("THUMB"));
                    double dx = e.HorizontalChange;
                    double dy = e.VerticalChange;

                    double oldWidth = parent.Width;
                    double oldHeight = parent.Height;
                    switch (subname)
                    {
                        case "TOPLEFT":
                            ResizeNoteLeft(note, parent, dx);
                            ResizeNoteTop(note, parent, dy);
                            break;
                        case "BOTTOMRIGHT":
                            ResizeNoteBottom(note, dy);
                            ResizeNoteRight(note, dx);
                            break;
                        case "BOTTOMLEFT":
                            ResizeNoteLeft(note, parent, dx);
                            ResizeNoteBottom(note, dy);
                            break;
                        case "TOPRIGHT":
                            ResizeNoteTop(note, parent, dy);
                            ResizeNoteRight(note, dx);
                            break;
                        case "LEFT":
                            ResizeNoteLeft(note, parent, dx);
                            break;
                        case "RIGHT":
                            ResizeNoteRight(note, dx);
                            break;
                        case "BOTTOM":
                            ResizeNoteBottom(note, dy);
                            break;
                        case "TOP":
                            ResizeNoteTop(note, parent, dy);
                            break;
                    }

                    string id = AppUtils.splitStringFromID(thumb.Name)[1];
                    if(id.Equals(""))
                        UpdateShadow(note as Panel);
                }

            }
        }


        internal void ChangeNoteColor(object sender, RoutedEventArgs e)
        {
            MenuFlyoutItem selectedItem = sender as MenuFlyoutItem;

            if (selectedItem != null)
            {
                string[] nameAndID = AppUtils.splitStringFromID(selectedItem.Tag.ToString());
                string noteID = nameAndID[1];

                Grid header = FindName("MenuStrip" + noteID) as Grid;
                RichEditBox textEntry = FindName("TextEntry" + noteID) as RichEditBox;
                if (header != null && textEntry != null)
                {
                    switch (nameAndID[0].ToUpper())
                    {
                        case "YELLOW":
                            header.Style = (Style) Application.Current.Resources["YellowHeaderTheme"];
                            textEntry.Style = (Style) Application.Current.Resources["YellowNoteTheme"];
                            break;
                        case "BLUE":
                            header.Style = (Style) Application.Current.Resources["BlueHeaderTheme"];
                            textEntry.Style = (Style) Application.Current.Resources["BlueNoteTheme"];
                            break;
                        case "RED":
                            header.Style = (Style) Application.Current.Resources["RedHeaderTheme"];
                            textEntry.Style = (Style) Application.Current.Resources["RedNoteTheme"];
                            break;
                        case "GREEN":
                            header.Style = (Style) Application.Current.Resources["GreenHeaderTheme"];
                            textEntry.Style = (Style) Application.Current.Resources["GreenNoteTheme"];
                            break;
                        case "PURPLE":
                            header.Style = (Style) Application.Current.Resources["PurpleHeaderTheme"];
                            textEntry.Style = (Style) Application.Current.Resources["PurpleNoteTheme"];
                            break;


                    }
                }
            }
        }

        internal void BringNoteToFront(object sender, RoutedEventArgs e)
        {
            UIElement element = sender as UIElement;
           
            if (element != null && !addNoteHandled)
            {
                string name = element.GetValue(NameProperty) as string;

                if (selectedElement != null)
                    Canvas.SetZIndex(selectedElement,-1);
                string id = AppUtils.splitStringFromID(name)[1];

                FloatingContent elementRoot = FindName("Note" + id) as FloatingContent;
                if(elementRoot == null)
                    Debug.WriteLine("FAIL: stickyNote root is not a FloatingContent");
                Canvas.SetZIndex(elementRoot, 1);
                selectedElement = elementRoot;
                Debug.WriteLine("note " + (id.Length == 0 ? "-1" : id) + " in front");
            }
            else if(!addNoteHandled)
            {
                Debug.WriteLine("FAIL WHALE: focused element is not a frameworkelement");
            }
            addNoteHandled = false;
        }

        private void FilterNotes(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            FloatingContent helperNote = FindName("Note") as FloatingContent;
            RichEditBox defaultEditBox = helperNote.FindName("TextEntry") as RichEditBox;
            string defaultBoxContent;

            defaultEditBox.Document.GetText(TextGetOptions.None, out defaultBoxContent);
            //check helper note
            bool helperHasText = defaultBoxContent.IndexOf(sender.Text, StringComparison.OrdinalIgnoreCase) >= 0;
            if (sender.Text.Length == 0)
            {
                helperNote.IsEnabled = true;
                helperNote.Opacity = 1;
                Canvas.SetZIndex(helperNote, selectedElement == helperNote ? 1 : -1);
            }
            //focus in front
            else if (helperHasText)
            {
                helperNote.IsEnabled = true;
                helperNote.Opacity = 1;
                Canvas.SetZIndex(helperNote, 1);
            }
            else
            {
                helperNote.IsEnabled = false;
                helperNote.Opacity = 0.35;
                Canvas.SetZIndex(helperNote, -2);
            }
            //escape if empty
            foreach (StickyNote note in notes)
            {
                if (note.Initialized)
                {
                    bool hasText = note.GetText().IndexOf(sender.Text, StringComparison.OrdinalIgnoreCase) >= 0;
                    //restore original state
                    if (sender.Text.Length == 0)
                    {
                        note.GetRoot().IsEnabled = true;
                        note.GetRoot().Opacity = 1;
                        Canvas.SetZIndex(note.GetRoot(), selectedElement == note.GetRoot() ? 1 : -1);
                    }
                    //focus in front
                    else if(hasText)
                    {
                        note.GetRoot().IsEnabled = true;
                        note.GetRoot().Opacity = 1;
                        Canvas.SetZIndex(note.GetRoot(), 1);
                    }
                    else
                    {
                        note.GetRoot().IsEnabled = false;
                        note.GetRoot().Opacity = 0.35;
                        Canvas.SetZIndex(note.GetRoot(), -2);
                    }
                }
            }
        }
        
        internal void RemoveNote(object sender, RoutedEventArgs e)
        {
            UIElement element = sender as UIElement;

            if (element != null)
            {
                string name = element.GetValue(NameProperty) as string;

                string id = AppUtils.splitStringFromID(name)[1];
                //spcial handle for "training note". make invisble and uninteractible until they need help again
                FloatingContent helpNote = FindName("Note") as FloatingContent;
                if (id.Length == 0)
                {
                    
                    helpNote.Opacity = 0.0;
                    helpNote.IsEnabled = false;
                    noteCount--;
                }
                else
                {
                    notes[System.Convert.ToInt32(id)].RemoveNoteFromScene(FindName("BaseCanvas") as Canvas);
                    noteCount--;
                }
                if (noteCount == 0)
                {
                    notes[0].AddNoteToScene(FindName("BaseCanvas") as Panel, 100, 100);
                    noteCount++;
                }

            }
            else
            {
                Debug.WriteLine("FAIL WHALE: cannot id note to delete");
            }
        }


        private void UpdateShadow(object sender)
        {
            Panel panel = sender as Panel;
            Canvas shadowCanvas = panel?.FindName("ShadowHolder") as Canvas;
            if (shadowCanvas != null)
            {
                shadowCanvas.Width = panel.ActualWidth;
                shadowCanvas.Height = panel.ActualHeight;

                Visual visual = ElementCompositionPreview.GetElementChildVisual(shadowCanvas);
                visual.Size = new Vector2((float)panel.ActualWidth + 2 * StickyNote.SHADOW_OFFSET, (float)panel.ActualHeight+ 2 * StickyNote.SHADOW_OFFSET);
            }
        }

        private bool isShiftPressed;
        private async void RootKeyProcess(CoreWindow coreWindow, KeyEventArgs e)
        {
            Debug.WriteLine("key tap!");
            
            if (e.VirtualKey == VirtualKey.F1)
            {
                FloatingContent helpNote = FindName("Note") as FloatingContent;
                if (!helpNote.IsEnabled)
                {
                    helpNote.Opacity = 1.0;
                    helpNote.IsEnabled = true;
                    //make sure new note is in front

                    Canvas.SetZIndex(selectedElement, -1);
                    Canvas.SetZIndex(helpNote, 1);
                    selectedElement = helpNote;
                    Debug.WriteLine("Note -1 in front");
                    noteCount++;
                }
            }

            if (e.VirtualKey == VirtualKey.Escape)
            {
                AutoSuggestBox box = FindName("NoteFilter") as AutoSuggestBox;
                if (box != null)
                {
                    box.Text = "";
                }
            }

            if (e.VirtualKey == VirtualKey.Shift && !isShiftPressed)
            {
                isShiftPressed = true;
                await Task.Delay(TimeSpan.FromMilliseconds(250));
                isShiftPressed = false;
            }
            //double tapped
            else if (e.VirtualKey == VirtualKey.Shift && isShiftPressed)
            {
                Debug.WriteLine("double key tap!");
                AutoSuggestBox box = FindName("NoteFilter") as AutoSuggestBox;
                box.Focus(FocusState.Programmatic);
            }
        }
    }
}
